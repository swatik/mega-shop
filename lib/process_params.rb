module ProcessParams
  def process_params(base_url)
    if (!session[:user_url])
      session[:user_url] = "&desc_order_by=ASC&view=list&order_by=name"
    end
    if (params[:order_by])
      session[:user_url].gsub!(/&order_by=[a-z]*/,'&order_by='+params[:order_by])
      base_url.gsub!(/order_by=[a-z]*&/,'')
      base_url.gsub!(/&order_by=[a-z]*/,'')
    end
    if (params[:desc_order_by])
      session[:user_url].gsub!(/desc_order_by=[A-Z]*&/,'desc_order_by='+params[:desc_order_by]+'&')
      base_url.gsub!(/desc_order_by=[A-Z]*&/,'')
    end
    #if (params[:per_page])
    #  session[:user_url].gsub!(/per_page=[0-9]*/,'per_page='+params[:per_page])
    #  base_url.gsub!(/&per_page=[0-9]*/,'')
    #end
    if (params[:view])
      session[:user_url].gsub!(/view=[a-z]*&/,'view='+params[:view]+'&')
      base_url.gsub!(/view=[a-z]*&/,'')
    end
    session[:user_url_processed] = "1"
    base_url+session[:user_url] #redirect_to somewhere with params
  end
end