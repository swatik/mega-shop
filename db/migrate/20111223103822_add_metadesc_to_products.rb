class AddMetadescToProducts < ActiveRecord::Migration
  def change
    add_column :products, :metadesc, :text
    add_column :products, :metatags, :text
  end
end
