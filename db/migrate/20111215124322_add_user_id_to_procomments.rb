class AddUserIdToProcomments < ActiveRecord::Migration
  def change
    add_column :procomments, :user_id, :integer
  end
end
