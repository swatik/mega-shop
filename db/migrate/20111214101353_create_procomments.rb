class CreateProcomments < ActiveRecord::Migration
  def change
    create_table :procomments do |t|
      t.string :name
      t.text :body
      t.references :product

      t.timestamps
    end
    add_index :procomments, :product_id
  end
end
