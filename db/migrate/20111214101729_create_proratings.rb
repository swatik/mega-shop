class CreateProratings < ActiveRecord::Migration
  def change
    create_table :proratings do |t|
      t.integer :value
      t.references :user
      t.references :product

      t.timestamps
    end
    add_index :proratings, :user_id
    add_index :proratings, :product_id
  end
end
