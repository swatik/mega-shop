class CreateShipMethods < ActiveRecord::Migration
  def change
    create_table :ship_methods do |t|
      t.string :method
      t.text :admin_comment

      t.timestamps
    end
  end
end
