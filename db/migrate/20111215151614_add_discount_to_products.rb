class AddDiscountToProducts < ActiveRecord::Migration
  def change
    add_column :products, :discount, :decimal
    remove_column :products, :sale_id
  end
end
