class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :user_id
      t.string :address
      t.text :admin_comment

      t.timestamps
    end
  end
end
