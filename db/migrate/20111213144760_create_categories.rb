class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.boolean :publish
      t.integer :parent_id
      t.text :admin_comment

      t.timestamps
    end
  end
end
