class AddHotToProducts < ActiveRecord::Migration
  def change
    add_column :products, :hot, :boolean
    add_column :products, :hit, :boolean
  end
end
