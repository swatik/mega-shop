class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :sku
      t.text :short_desc
      t.text :desc
      t.integer :price
      t.integer :picture_id
      t.boolean :publish
      t.text :admin_comment

      t.timestamps
    end
    add_column :products, :full_image, :string
    add_column :products, :thumb_image, :string
    add_column :products, :in_stock, :integer
    add_column :products, :available_date, :datetime
    add_column :products, :sales, :integer
  end
end
