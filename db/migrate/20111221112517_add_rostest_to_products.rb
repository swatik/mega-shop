class AddRostestToProducts < ActiveRecord::Migration
  def change
    add_column :products, :rostest, :boolean
    add_column :products, :action, :boolean
    add_column :products, :new, :boolean
  end
end
