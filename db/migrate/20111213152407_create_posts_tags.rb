class CreatePostsTags < ActiveRecord::Migration
  def change
    create_table :posts_tags, :id => true do |t|
      t.references :post, :tag
    end
  end
end
