class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.integer :address_id
      t.integer :ship_method_id
      t.text :comment
      t.text :admin_comment

      t.timestamps
    end
  end
end
