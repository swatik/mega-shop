# encoding: utf-8
module ApplicationHelper
  
  def main_categories
    Category.find_all_by_parent_id("0",:order => 'list_order')
  end
  
  def count_cart
    if user_signed_in?
      @cart_order = Order.find_by_user_id_and_address_id(current_user[:id],nil)
    else
      @cart_order = Order.find_by_session_id_and_address_id(request.session_options[:id],nil)
    end
    @prod_count = 0
    @cart_total_price = 0
    if (@cart_order != nil)
      @cart_order.orders_products.each do |pd|
        @prod_count += pd["count"]
      end
      @cart_order.orders_products.each do |prod|
        pre_total = prod.price*prod.count
        @cart_total_price += pre_total
      end
    end
    logger.info "AAAA"
    return @prod_count, @cart_total_price
  end
  

  def have_prods?
    @prod_count, @cart_total_price = count_cart
    logger.info @prod_count
    logger.info @cart_total_price
    @prod_count != 0
  end


  def curr_cat(id)
    Category.find_by_id(id)
  end

end
