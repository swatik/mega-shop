#encoding: utf-8
class OrderMailer < ActionMailer::Base
  default from: "order@megaololo.jp"

  def new_user_and_new_order(user, password, order)
    @order = order
    @user = user
    @password = password
    mail(to: user.email, subject: "Вы сделали заказ на ololo.ru")
  end

  def new_order(order)
    @order = order
    @user = current_user
    mail(to: user.email, subject: "Вы сделали заказ на сайте GBstore.ru.")
  end
end
