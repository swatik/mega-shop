class Category < ActiveRecord::Base
  has_many :categories_products
  has_many :products, :through => :categories_products
  
  def search(q)
    @search_result = Array.new
    @counter = Array.new
    count_prod = 0
    check_cat = false
      @categories = Category.all
      @categories.each do |c|
        count_prod = c.products.find(:all, :conditions => ["`name` LIKE ? OR `short_desc` LIKE ? OR `desc` LIKE ?", "%#{q}%", "%#{q}%", "%#{q}%"]).count
        if count_prod > 0
          check_cat = true
        end
        if check_cat == true 
          @search_result.push(c)
          @counter.push(count_prod)
          count_prod = 0
        end
        check_cat = false
      end
    return @search_result, @counter
  end
end