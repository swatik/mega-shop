class Order < ActiveRecord::Base
    paginates_per 5
    belongs_to :ship_method
    belongs_to :user
    belongs_to :address
    has_many :orders_products
    has_many :products, :through => :orders_products

    validates :user_id, :address_id, :presence => true
end
