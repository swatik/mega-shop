class Product < ActiveRecord::Base
  has_many :orders_products
  has_many :orders, :through => :orders_products
  
  
  
  has_many :categories_products
  has_many :categories, :through => :categories_products
  
  validates :name, :presence => true
  def to_param
    "#{id}-#{categories.first.name.parameterize}-#{name.parameterize}"
  end
  
  def search(q, desc_order_by, order_by)
    @search = Product.find(:all, :conditions => ["`name` LIKE ? OR `short_desc` LIKE ? OR `desc` LIKE ?", "%#{q}%", "%#{q}%", "%#{q}%"], :order => "#{order_by} #{desc_order_by}")
    return @search
  end
  
  def search_by_section(section, q, desc_order_by, order_by)
    @category = Category.find_by_id(section)
    @search = @category.products.find(:all, :conditions => ["`name` LIKE ? OR `short_desc` LIKE ? OR `desc` LIKE ?", "%#{q}%", "%#{q}%", "%#{q}%"], :order => "#{order_by} #{desc_order_by}")
     return @search
  end
end
