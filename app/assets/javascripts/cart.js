$(document).ready(function () { 
  $(window).bind('beforeunload', function(){
    if (window.location.pathname != "/cart/edit") {
      return;
    }
    var f = $('#cart-form');
    $.ajax({
      async: false,
      url: "/cart",
      type: "PUT",
      dataType: "html",
      data: f.serialize(),
      complete: function() {
      },
      success: function(data) {
      },
    });
  });
  $(".cart_product_block input[type='text']").keyup( function(event) {
    var val = $(this).val(); 
    val = parseInt(val);
    if (val >0 && val < 100) {
        $(this).removeClass('error');
        var price = $(this).attr('price-value');
        //set price of this item
        $(this).parent().parent().find('.cart_product_price').html(price*val+'руб');
        //recalc all items
        var total= 0;
        $(".cart_product_block input[type='text']").each(function(index) {
          total += $(this).attr('price-value') * parseInt($(this).val());
        });
        $('.amount_price').removeClass('error');
        $('.amount_price').html(total+'руб');
      }
     else {
       $(this).addClass('error');
        $(this).parent().parent().find('.cart_product_price').html('Некорректное количество');
        $('.amount_price').addClass('error');
     }
  });

});
