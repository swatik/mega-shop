$(document).ready(function(){
  $('.star').raty({
    start: $('.star-data').attr('data-value'),
    readOnly: $('.star-data').attr('data-is-voted') == 'true',
    click: function(score, evt) {
      if ($('.star-data').attr('data-user-signed') == 'true') {
      var f = $('.vote-form');
      $('.vote-form input[name=rating]').val(score);
      $.ajax({
        url: f.attr('action'),
        type: f.attr('method'),
        dataType: "html",
        data: f.serialize(),
        complete: function() {
          $('.star').raty('readOnly',true);
        },
        success: function(data) {
          $('.star').raty('start', data);
        },
      })
      }
      else {
      $('#pls-reg').html("Пожалуйста, зарегистрируйтесь");
      }
    }
  });
});