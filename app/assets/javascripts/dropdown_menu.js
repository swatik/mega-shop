
$(document).ready(function () { 
  $('body').hover(function (e) {
    if ($('#top-nav .horiz').hasClass('hover')) {
      $('#top-nav ul').slideUp(300);
      $('#top-nav .hover').removeClass('hover');
    }
  });
  $('body').click(function (e) {
    if ($('#top-nav .horiz').hasClass('hover')) {
      $('#top-nav ul').slideUp(300);
      $('#top-nav .hover').removeClass('hover');
    }
  });
  $('#top-nav .horiz').hover(
    function (e) {
    e.stopPropagation();
    e.preventDefault();
    if ($(this).hasClass('hover')) {  
    }
    else {
      $('#top-nav ul').slideUp(300);
      $('#top-nav .hover').removeClass('hover');
      $('ul', $(this).parent()).slideDown(300);
      $(this).addClass('hover');
    }
  });
});