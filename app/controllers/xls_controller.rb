class XlsController < ApplicationController
before_filter :authenticate_admin!
  def index
    @categories = Category.find_all_by_parent_id(0)
  end
  
  def create
    @product = Product.find_by_id(params[:id])
    @product.name = params[:name]
    @product.price = params[:price]
    @product.hit = params_check(params[:hit])
    @product.discount = params[:discount]
    @product.rostest = params_check(params[:rostest])
    @product.action = params_check(params[:active])
    @product.new = params_check(params[:new])
    @product.save
    respond_to do |format|
        format.html { redirect_to('/xls') }
    end 
  end
  
  def params_check(q)
    if q == 'yes'
      check = true
    else
      check = false
    end
    return check
  end
end
