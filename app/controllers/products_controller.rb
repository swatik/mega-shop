class ProductsController < ApplicationController
  def show
    set_tab :catalog
    @product = Product.find(params[:id])
    @check = false
    @category = Array.new
    count = 0
    ck = Array.new
    ckk = false
    prev_id = 0
    if @product.categories.count > 1
      @product.categories.each do |c|
        ck = checkin(c, @product.id)
        ck.each do |k|
          if k == true
            ckk = true
          end
        end
        if ckk == false
          @category[count] = c
          count ||= 0
          count += 1
        end
        ckk = false
      end
    end    
    @breadcrumb_parents = Array.new
    @breadcrumb_childrens = Array.new
    counter1 = 0
    counter2 = 0
    @product.categories.each do |c|
      @cat_ch = Category.find_by_parent_id(c.id)
      if @cat_ch != nil
        @breadcrumb_parents[counter1] = c
        counter1 ||= 0
        counter1 += 1
      else
        @breadcrumb_childrens[counter2] = c
        counter2 ||= 0
        counter2 += 0
      end
    end
  end
  
  def checkin(c, id)
    ck = Array.new
    counter = 0
    @cat = Category.find_all_by_parent_id(c.id)
    if @cat != nil
      @cat.each do |ca|
        ca.products.each do |cate|
          if cate.id == id
            ck[counter] = true
          end
        end
      end
    end
    return ck
  end
end
