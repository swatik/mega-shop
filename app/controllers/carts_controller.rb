#encoding: utf-8
class CartsController < ApplicationController

  def edit

  end

  def add_product
    if user_signed_in?
      finded_order = Order.find_by_user_id_and_address_id(current_user.id,nil)
      if !finded_order
        cur_ord = Order.new
        cur_ord.user_id = current_user[:id]
        cur_ord.session_id = request.session_options[:id]
      else
        cur_ord = finded_order
        cur_ord.session_id = request.session_options[:id]
      end
    else
      finded_order = Order.find_by_session_id_and_address_id(request.session_options[:id],nil)
      if !finded_order
        cur_ord = Order.new
        cur_ord.session_id = request.session_options[:id]
      else
        cur_ord = finded_order
      end
    end

    finded_product = OrdersProduct.find_by_product_id_and_order_id(params[:item_id],cur_ord[:id])
    if !finded_product
      cur_ord_prod = cur_ord.orders_products.new
      cur_ord_prod.product_id = params[:item_id]
      prod = Product.find_by_id(params[:item_id])
      cur_ord_prod.price = prod.price
      cur_ord_prod.count = 1
      cur_ord.save
      cur_ord_prod.order_id = cur_ord.id
    else
      cur_ord_prod = finded_product
      cur_ord_prod.count += 1
      cur_ord.save
    end
    cur_ord_prod.save

    redirect_to '/cart/edit'
  end


  def update
    @quantity_arr = Array.new
    params[:cart].each do |cart|
      if (cart[1].to_i > 0 and cart[1].to_i < 50)
        prod = OrdersProduct.find_by_id(cart[0])
        prod["count"] = cart[1]
        prod.save
      end
    end
    respond_to do |format|
      render :text => "ok"
    end
  end

  def remove_product
    OrdersProduct.delete(params[:id])
    redirect_to :back
  end

  def checkout
    if !user_signed_in?
      deny_access
    end
    if @prod_count == 0
      flash[:notice] = "Пожалуйста, закажите что-нибудь, прежде чем приступать к оформлению"
      redirect_to :back
    end
  end

  def checkout_finish
    if (params[:name]=="" || params[:phone]=="" || params[:email]=="" || params[:address]=="" || params[:ship_method]=="")
      flash[:notice] = "Вы ввели не все необходимые данные"
      redirect_to :back
      return
    else
      if !user_signed_in?
        ord = Order.find_by_session_id_and_address_id(request.session_options[:id],nil)
        user = User.new
        @pass = random_password
        user.password = @pass
        user.password_confirmation = @pass
        entered_mail = params[:email]
        find_email = User.find_by_email(entered_mail)
        if (find_email)
          flash[:notice] = "Пользователь с таким e-mail адресом уже зарегистрирован"
          redirect_to :back
          return
        else
          user.email = params[:email]
        end
        flash[:notice] = "Ваш заказ обработан. Ваш пароль: #@pass"
      else
        ord = Order.find_by_user_id_and_address_id(current_user[:id],nil)
        user = current_user
        flash[:notice] = "Ваш заказ обработан."
        user.email = params[:email]
      end
      user.name = params[:name]
      user.phone = params[:phone]
      user.save

      ord.user_id = user.id

      if params[:address] == "Другой.."
        adr = Address.new
        adr.user_id = user.id
        adr.address = params[:new_address]
        adr.save
      else
        adr = Address.find_by_address(params[:address])
      end
      ord.address_id = adr.id

      shipm = ShipMethod.find_by_method(params[:ship_method])
      ord.ship_method_id = shipm.id

      ord.save

      #OrderMailer.new_user_and_new_order(user, @pass, ord).deliver
      redirect_to shop_index_path
    end
  end

  def random_password(size = 3)
    c = %w(b c d f g h j k l m n p qu r s t v w x z ch cr fr nd ng nk nt ph pr rd sh sl sp st th tr)
    v = %w(a e i o u y)
    f, r = true, ''
    (size * 2).times do
      r << (f ? c[rand * c.size] : v[rand * v.size])
      f = !f
    end
    r
  end

end
