# encoding: utf-8
class ApplicationController < ActionController::Base
  include SessionsHelper
  protect_from_forgery
  

  
  def after_sign_in_path_for(resource_or_scope)
    case resource_or_scope
    when :user, User
      cur_ord = Order.find_by_session_id_and_address_id(request.session_options[:id],nil)
      nonfinished_ord = Order.find_by_user_id_and_address_id(current_user.id,nil)
      if nonfinished_ord
        nonfinished_ord.delete
      end
      if cur_ord != nil
        cur_ord.user_id = current_user.id
        cur_ord.save
      end
      store_location = session[:return_to]
      clear_stored_location
      (store_location.nil?) ? profile_index_path : store_location.to_s
      
    else
      super
    end
  end
  
  

end
