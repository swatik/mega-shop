class PriceController < ApplicationController
  def index
    set_tab :price
    @categories = Category.find_all_by_parent_id(0)
  end
end
