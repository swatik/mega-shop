class ProfileController < ApplicationController
  set_tab :profile
before_filter :authenticate_user!
  def index
    @addrs = Address.find_all_by_user_id(current_user.id)
    @user_orders = Kaminari.paginate_array(current_user.orders).page(params[:page]).per(5)
  end
  
  def edit_addrs
    @addrs = Address.find_by_id(params[:id])
  end
  
  def new_addrs
  end
  
  def show_order
    @order = Order.find_by_id(params[:id])
    @total_price = 0
    @prod_count = 0
    @order.orders_products.each do |pd|
      @prod_count += pd["count"]
    end
    @order.orders_products.each do |prod|
      pre_total = prod.price*prod.count
      @total_price += pre_total
    end
  end
  
  def new_addrs_add
    @adr = Address.new
    @adr.user_id = current_user.id
    @adr.address = params[:address]
    @adr.save
    respond_to do |format|
        format.html { redirect_to('/profile/index') }
    end
  end
  
  def update
    @user = User.find_by_id(current_user.id)
    @user.name = params[:name]
    @user.phone = params[:phone]
    @user.email = params[:email]
    @user.save
    respond_to do |format|
        format.html { redirect_to('/profile/index') }
    end
  end
  
  def addrs_update
    @adr = Address.find_by_id(params[:address_id])
    if @adr.user_id == current_user.id
      @adr.address = params[:address]
      @adr.save
      respond_to do |format|
        format.html { redirect_to('/profile/index') }
      end
    else
      respond_to do |format|
        format.html { redirect_to('/profile/index', :notice => 'I TOLD YOU TO STOP IT DAWG, I TOLD YOU') }
      end
    end
  end
end
