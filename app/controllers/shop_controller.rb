class ShopController < ApplicationController
  include ProcessParams
    
  def index
    @products = Product.all
  end
  
  def sitemap
    set_tab :sitemap
    @categories = Category.find_all_by_parent_id(0)
  end
    
  def show_cat 
    if (session[:user_url_processed] == "0" || !session[:user_url_processed] || params[:order_by] == nil || params[:desc_order_by] == nil)
      redirect_to process_params(request.url)
      return
    end
    session[:user_url_processed] = "0"
    set_tab :catalog
    @cat_ch = Array.new
    @cat = Category.find_by_id(params[:id])
    @cat_children = Category.find_by_parent_id(params[:id])
    @all = Category.all
    @all.each do |a|
      @cat_ch = Category.find_all_by_parent_id(a.id)
      @cat_ch.each do |ch|
        if @cat == ch
          @cat_parent = a
        end
      end
    end
    
    @cat = Category.find_by_id(params[:id])
    @cat_products = @cat.products.order(params[:order_by]+" "+params[:desc_order_by]).page(params[:page]).per(params[:per_page])
  end
  
  def show_parent_cat
    set_tab :catalog
    @cat = Category.find_by_id(params[:id])
    @cat_children = Category.find_all_by_parent_id(params[:id])
    @all = Category.all
    @all.each do |a|
      @cat_ch = Category.find_by_parent_id(a.id)
      if @cat == @cat_ch
        @cat_parent = a
      end
    end
    case @cat.id
    when 1
      set_tab :notebooks
    when 2
      set_tab :tablet
    when 3
      set_tab :computers
    when 4
      set_tab :phones
    when 5
      set_tab :players
    when 6
      set_tab :accessories
    end
  end
  
  def find
    set_tab :catalog
    if (session[:user_url_processed] == "0" || !session[:user_url_processed] || params[:order_by] == nil || params[:desc_order_by] == nil)
      redirect_to process_params(request.url)
      return
    end
    session[:user_url_processed] = "0"
    if params[:section] == nil
      @search_result, @counter = Category.new.search(params[:q])
      @alter_search = Kaminari.paginate_array(Product.new.search(params[:q], params[:desc_order_by], params[:order_by])).page(params[:page]).per(params[:per_page])
    else
      @search_result = Array.new
      @counter = Array.new
      @alter_search = Kaminari.paginate_array(Product.new.search_by_section(params[:section], params[:q], params[:desc_order_by], params[:order_by])).page(params[:page]).per(params[:per_page])
    end  
  end
  
  def catalog
    set_tab :catalog
  end
  
end
