require 'test_helper'

class OrderTest < ActiveSupport::TestCase
   test "order must have user id" do
     order = orders(:one)
     assert !order.save
   end

   test "order must have address id" do
     order = orders(:two)
     assert !order.save
   end 

   test "save right order" do
     order = orders(:three)
     assert order.save
   end 
end
