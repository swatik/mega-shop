require 'test_helper'

class ProductTest < ActiveSupport::TestCase
   test "product must have name" do
     product = products(:one)
     assert !product.save
   end

   test "save right product" do
     product = products(:two)
     assert product.save
   end

end
